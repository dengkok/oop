<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("shaun");

echo "Nama Binatang : $sheep->name <br>"; // "shaun"
echo "Jumlah kaki : $sheep->legs <br>"; // 4
echo "Cols Blooed : $sheep->cold_blooded <br>"; // "no"
echo "<br>";
echo "<br>";

$kodok = new Frog("buduk");
echo "Nama Binatang : $kodok->name <br>";
echo "Jumlah kaki : $kodok->legs <br>";
echo "Cols Blooed : $kodok->cold_blooded <br>";
$kodok->jump(); // "hop hop"
echo "<br>";
echo "<br>";

// index.php
$sungokong = new Ape("kera sakti");
echo "Nama Binatang : $sungokong->name <br>";
echo "Jumlah kaki : $sungokong->legs <br>";
echo "Cols Blooed : $sungokong->cold_blooded <br>";
$sungokong->yell(); // "Auooo"


?>